package id.findfamily.target;

public interface ApplicationConstants {
    static final String HTTP_URL = "http://findfamily.idekreasi.net/";
    static final Integer DB_VERSION = 1;
}
