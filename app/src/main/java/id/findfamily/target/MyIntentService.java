package id.findfamily.target;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

public class MyIntentService extends IntentService {
    private static final String TAG = "MyIntentService";
    private GPSTracker gps;
    private double lat, lon;
    private String imei="123", parambody;
    private Handler mHandler;

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String parambody, tvid, idmedia, status, tstamp;
        // Do the task here
        Log.d(TAG, "Service running");
        // Sent Data Impression internal storage to server

        gps = new GPSTracker(this);
        Log.d("adie", "check gps...");
        // check if GPS enabled
        if (gps.canGetLocation()) {
            Log.d("adie", "gps enable");
            Log.d("adie", gps.toString());
            lat = gps.getLatitude();
            lon = gps.getLongitude();
            Log.d("adie", "Your Location is - \nLat: "
                    + lat + "\nLong: " + lon);
            sendata();
        } else {
            Log.d("adie", "gps disable");
            gps.showSettingsAlert();
        }

        sendata();
    }

    public void sendata(){
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                //checkin_latlong.setText(lat+ ","+lon);
                CheckConnection checkConn = new CheckConnection();
                Log.d("adie", "check connection");
                if (checkConn.isConnected(getApplicationContext())) {
                    callapi();
                }
            }
        });
    }

    public void callapi(){
        Log.d("adie", "call api ");
        parambody = "imei=123" +"&lat="+lat+"&lon="+lon;
        //parambody = "imei=123&lat=-7.2729388&lon=112.7427956";
        Log.d("adie", "parameter " + parambody);
        //new GetResponseFromOkHTTP(this, "load data", "json", "getlocation.php?", parambody, "", "").execute();
        //String result = ServiceCall.doServerCall("GET", "http://findfamily.idekreasi.net/getlocation.php?imei=findfamily&lat="+lat+"&lon="+lon, "");
        Log.d("adie", "finish call api ");
    }

}
