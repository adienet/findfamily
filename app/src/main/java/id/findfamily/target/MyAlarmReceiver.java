package id.findfamily.target;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import id.findfamily.target.localdata.DataUser;

import static android.support.constraint.Constraints.TAG;

public class MyAlarmReceiver extends BroadcastReceiver implements AsyncResponse {
    public static final int REQUEST_CODE = 12345;
    public static final String ACTION = "id.findfamily.target";
    public static GPSTracker gps;
    public double lat, lon;
    private String imei = "", parambody;
    private Handler mHandler;
    private TelephonyManager telephonyManager;
    private DataUser ds;

    // Triggered by the Alarm periodically (starts the service to run task)
    @SuppressLint("MissingPermission")
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("adie", "receiver");
        Intent i = new Intent(context, MyIntentService.class);
        i.putExtra("foo", "bar");
        ds = new DataUser(context);
        callapi(context, intent);
    }

    public void callapi(final Context context, Intent intent){
        String parambody, tvid, idmedia, status, tstamp;
        // Do the task here
        Log.d(TAG, "Service running");
        // Sent Data Impression internal storage to server

        gps = new GPSTracker(context);
        Log.d("adie", "check gps...");
        // check if GPS enabled
        if (gps.canGetLocation()) {
            Log.d("adie", "gps enable");
            Log.d("adie", gps.toString());
            lat = gps.getLatitude();
            lon = gps.getLongitude();
            Log.d("adie", "Your Location is - \nLat: "
                    + lat + "\nLong: " + lon);

        } else {
            Log.d("adie", "gps disable");
            gps.showSettingsAlert();
        }


        //checkin_latlong.setText(lat+ ","+lon);
        CheckConnection checkConn = new CheckConnection();
        Log.d("adie", "check connection");
        if (checkConn.isConnected(context)) {
            callapi();
        }
    }


    public void callapi(){
        Log.d("adie", "call api ");
        parambody = "imei=" + ds.getDataUser("imei") +"&lat="+lat+"&lon="+lon;
        //parambody = "imei=123&lat=-7.2729388&lon=112.7427956";
        Log.d("adie", "parameter " + parambody);
        new GetResponseFromOkHTTP(this, "load data", "json", "getlocation.php?", parambody, "", "").execute();
        //String result = ServiceCall.doServerCall("GET", "http://findfamily.idekreasi.net/getlocation.php?imei=findfamily&lat="+lat+"&lon="+lon, "");
        Log.d("adie", "finish call api ");
    }

    @Override
    public void processStart(String message) {

    }

    @Override
    public void processFinish(String flag, String output) {
        Log.d("adie", "finish call api "+output);
    }

    @Override
    public void processFailure(String output) {

    }
}
