package id.findfamily.target.localdata;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * class DataUser digunakan untuk menyimpan semua konfigurasi parameter aplikasi
 */

public class DataUser {

    Context context = null;
    private SQLiteDatabase db;
    private Data dbData;
    Cursor cData;
    private static final String TAG = "DataUser";

    public DataUser(){}

    public DataUser(Context ctx){
        context = ctx;
    }

    /**
     * method setDataUser digunakan untuk mengisi nilai parameter konfigurasi
     * @param paramkey   Key dari parameter konfigurasi
     * @param paramvalue nilai dari parameter konfigurasi
     */
    public void setDataUser(String paramkey, String paramvalue){
        try {
            dbData = new Data(context);
            db = dbData.getWritableDatabase();
            db.execSQL("delete from data_user where paramkey='"+paramkey+"'");
            db.execSQL("insert into data_user values ('" + paramkey + "','" + paramvalue + "')");
            db.close();
            dbData.close();
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * function getDataUser digunakan untuk mendapatkan nilai parameter konfigurasi berdasarkan key parameter
     * @param paramkey   Key dari parameter
     */
    public String getDataUser(String paramkey){
        try{
            dbData = new Data(context);
            db = dbData.getReadableDatabase();
            String userApi = "-";
            String[] params = new String[]{ paramkey };
            cData = db.rawQuery("SELECT paramvalue FROM data_user where paramkey =?", params);
            if(cData!=null) {
                if (cData.getCount() >= 1) {
                    if (cData != null && cData.moveToFirst()) {
                        userApi = cData.getString(0);
                        closeSQL();
                    } else {
                        userApi = "-";
                        closeSQL();
                    }
                }

            }else {
                userApi = "-";
                closeSQL();
            }
            return userApi;
        } catch (SQLException e){
            return "-";
        }
    }

    /**
     * method closeSQL digunakan untuk menutup koneksi database sqllite
     */
    public void closeSQL(){
        if(db.isOpen()) db.close();
        if(!cData.isClosed()) db.close();
        dbData.close();
    }
}
