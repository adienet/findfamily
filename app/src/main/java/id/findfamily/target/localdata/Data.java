package id.findfamily.target.localdata;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import id.findfamily.target.ApplicationConstants;

/**
 * class Data digunakan untuk database sqlite
 */

public class Data extends SQLiteOpenHelper {

    public Data(Context context) {
        super(context, "findfamily.db", null, ApplicationConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS data_user(paramkey VARCHAR, paramvalue VARCHAR);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }

}