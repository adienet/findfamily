package id.findfamily.target;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class GetResponseFromOkHTTP extends AsyncTask<String, Void, String> {
    public AsyncResponse delegate=null;
    String messageOnPreExecute;
    String typeOutput;
    OkHttpClient client;
    private Exception exception;
    MediaType JSON;
    String urlheader, parameterbody, userauth, passwordauth;

    public GetResponseFromOkHTTP(AsyncResponse asyn, String msg, String type, String url, String parameter, String userid, String password){
        delegate = asyn;
        messageOnPreExecute = msg;
        typeOutput = type;
        urlheader = url;
        parameterbody = parameter;
        userauth = userid;
        passwordauth = password;
        client = new OkHttpClient.Builder()
                .connectTimeout(0, TimeUnit.SECONDS)
                .build();

    }

    protected void onPreExecute() {
        delegate.processStart(messageOnPreExecute);
    }

    @Override
    protected String doInBackground(String... urls) {
        try {
            String getResponse = post(ApplicationConstants.HTTP_URL,parameterbody);

            return getResponse;
        } catch (Exception e) {
            this.exception = e;
            return null;
        }
    }

    protected void onPostExecute(String result) {
        if (result!=null) {
            Log.d("api:", result);
        }
        delegate.processFinish("", result);
    }

    public String post(String url, String json) throws IOException {
        String credential = Credentials.basic(userauth,passwordauth);
        RequestBody body = RequestBody.create(JSON, json);
        Log.d("API:", url+urlheader+json);
        //Log.d("API:", userauth +" : "+passwordauth);
        //Log.d("API:", json);
        Request request = new Request.Builder()
                .cacheControl(new CacheControl.Builder().noCache().build())
                .url(url+urlheader+json)
                .addHeader("Authorization",credential)
                .addHeader("url",urlheader)
                //.post(body)
                .build();

        //Log.d("API","timeout"+client.connectTimeoutMillis());
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}

