package id.findfamily.target;

public interface AsyncResponse {
    void processStart(String message);
    void processFinish(String flag, String output);
    void processFailure(String output);
}
